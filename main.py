import math


def main():
    """Главный метод программы"""

    input('Нажмите Enter что бы выбрать формулу')
    print('1) G = (10 * (-45 * a^2 + 49 * a * x + 6 * x^2)) / (15 * a^2 + 49 * a * x + 24 * x^2)')
    print('2) F = tan(5 * a^2 + 34 * a * x + 45 * x^2)')
    print('3) Y = -1 * asin(7 * a^2 - a * x - 8 * x^2)')
    p = input()
    if p == '1':
        calc_g()
    elif p == '2':
        calc_f()
    elif p == '3':
        calc_y()
    else:
        print('Попробуйте еще раз')


def get_user_input():
    """Функция пользовательского ввода"""

    string = input('Введите a: ')
    try:
        a = float(string)
    except ValueError:
        print('Ошибка при вводе а')
        exit()

    string = input('Введите x: ')
    try:
        x = float(string)
    except ValueError:
        print('Ошибка при вводе x')
        exit()
    return a, x


def calc_g():
    """Функция подсчета формулы G"""

    data = get_user_input()
    try:
        g = (10 * (-45 * data[0] ** 2 + 49 * data[0] * data[1] + 6 * data[1] ** 2)) / (
                15 * data[0] ** 2 + 49 * data[0] * data[1] + 24 * data[1] ** 2)
    except ZeroDivisionError:
        g = 'Попытка деления числа на ноль'

    print(f'Даны значения: a = {data[0]} и x = {data[1]}')
    print(f'Ответы: G = {g}')
    input("Нажмите Enter для продолжения")


def calc_f():
    """Функция подсчета формулы F"""

    data = get_user_input()
    f = math.tan(math.radians(5 * data[0] ** 2 + 34 * data[0] * data[1] + 45 * data[1] ** 2))

    print(f'Даны значения: a = {data[0]} и x = {data[1]}')
    print(f'Ответы: F = {f}')
    input("Нажмите Enter для продолжения")


def calc_y():
    """Функция подсчета формулы Y"""

    data = get_user_input()
    try:
        y = -1 * math.asin(math.radians(7 * data[0] ** 2 - data[0] * data[1] - 8 * data[1] ** 2))
    except ValueError:
        y = 'Ошибка'

    print(f'Даны значения: a = {data[0]} и x = {data[1]}')
    print(f'Ответы: Y = {y}')
    input("Нажмите Enter для завершения")


main()
